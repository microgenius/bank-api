<a name="readme-top"></a>

<div align="center">

  <h1><b>Bank API</b></h1>

</div>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
    - [Key Features](#key-features)
- [💻 Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Setup](#setup)
  - [Install](#install)
  - [Usage](#usage)
  - [Run tests](#run-tests)
  - [Deployment](#deployment)
- [👥 Authors](#authors)
- [🔭 Future Features](#future-features)
- [🤝 Contributing](#contributing)
- [⭐️ Show your support](#support)
- [🙏 Acknowledgements](#acknowledgements)
- [📝 License](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 Bank-API <a name="about-project"></a>

**Bank-API** is a simple API that manages user bank accounts. It display the transaction made by the user and it's fully CRUD for all endpoints.

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

<details>
  <summary>Server</summary>
  <ul>
    <li><a >NestJS</a></li>
  </ul>
</details>

<!-- Features -->

### Key Features <a name="key-features"></a>

- **User sign-in**
- **User sign-up**
- **User sign-out**

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<p align="right">(<a href="#readme-top">back to top</a>)</p>


## API documentation <a name="live-demo"></a>

```sh
http://localhost:3000 - this is the root route

http://localhost:3000/users - this route is used to get/post user

http://localhost:3000/users/1 - this route is used to get/delete user

http://localhost:3000/cards - this route is used to get/post cards

http://localhost:3000/cards/1 - this route is used to get/delete card

http://localhost:3000/cards/1/transactions - this route is used to get transactions

http://localhost:3000/cards/1/deposit - this route is used to post deposit

http://localhost:3000/cards/1/withdraw - this route is used to delete withdraw

```

<p align="right">(<a href="#readme-top">back to top</a>)</p>

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, follow these steps.

### Prerequisites

In order to run this project you need to have :

- NodeJS installed
- NestJS installed
- Dependencies that project uses

### Setup

Clone this repository to your desired folder:

```sh
  
  cd my-folder
  
  git clone https://gitlab.com/microgenius/bank-api.git

```

```sh
  
  cd bank-api

```

### Install

Install this project gems with:

```sh

  npm install

```

### Usage

To run the project, execute the following command:

```sh

  npm start

```

### Run tests

To run tests, run the following command:

- For eslint run the following:

- For Stylelint(CSS) run the following:

```sh
  npx stylelint "**/*.{css,scss}"
```

- For testing the project run the following:

```sh
  npm run test
```

<!-- AUTHORS -->

## 👥 Author <a name="authors"></a>

👤 **Sandeep Ghosh**

- GitHub: [mailsg](https://github.com/mailsg)
- LinkedIn: [Sandeep Ghosh](https://www.linkedin.com/in/sandeep0912/)

<!-- FUTURE FEATURES -->

## 🔭 Future Features <a name="future-features"></a>

- **Enhanced UI**
- **Admin user**
- **Authorization**

<!-- CONTRIBUTING -->

## 🤝 Contributing <a name="contributing"></a>

Contributions, issues, and feature requests are welcome!

Feel free to check the [issues page](https://gitlab.com/microgenius/bank-api/issues).

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- SUPPORT -->

## ⭐️ Show your support <a name="support"></a>

If you encounter any issues or have questions about the project, feel free to reach out for support.Checkout the social links included in the authors, contact any of one or simply contribute this repo by raising a pull request on and always try to check the issues page.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- ACKNOWLEDGEMENTS -->

## 🙏 Acknowledgments <a name="acknowledgements"></a>

We would like to express our heartfelt appreciation to anyone who contributes to this project and also out dear developers wo are working to develop this product and make it work

## 📝 License <a name="license"></a>

This project is [MIT](./MIT.md) licensed.

<p align="right">(<a href="#readme-top">back to top</a>)</p>
